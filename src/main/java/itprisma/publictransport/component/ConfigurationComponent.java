package itprisma.publictransport.component;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@Setter
@Getter
@ConfigurationProperties("custom-properties")
public class ConfigurationComponent {
    private Integer defaultPageLimit;
}
