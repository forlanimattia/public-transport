package itprisma.publictransport.persistence;

import itprisma.publictransport.model.Train;
import itprisma.publictransport.model.Wagon;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface WagonRepository extends JpaRepository<Wagon, Long> {
    Page<Wagon> findByTrain(Train train, Pageable pageable);

    @Query("SELECT w from Wagon w where w.powerSupply = :powerSupply")
    Page<Wagon> findByPowerSupply(Pageable pageable, @Param("powerSupply") Wagon.PowerSupply powerSupply);
}
