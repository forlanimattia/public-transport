package itprisma.publictransport.persistence;

import itprisma.publictransport.model.Train;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TrainRepository extends JpaRepository<Train,Long> {
    boolean existsByNameAndIdIsNot(String name, Long id);
    boolean existsByName(String name);
    @Query(value = "select t.* from train t " +
            "join train_routes tr on t.id = tr.id_train " +
            "join routes r on r.id = tr.id_routes " +
            "where r.arrival_station = :arrival ", nativeQuery = true)
    Page<Train> findAllByRoutesIsLike(String arrival, Pageable pageable);
}
