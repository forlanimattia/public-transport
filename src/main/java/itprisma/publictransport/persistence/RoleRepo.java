package itprisma.publictransport.persistence;

import itprisma.publictransport.domain.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepo extends JpaRepository<Role,Long>{
    Role findByName(String name);
}