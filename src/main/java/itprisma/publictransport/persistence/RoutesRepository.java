package itprisma.publictransport.persistence;

import itprisma.publictransport.model.Routes;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoutesRepository extends JpaRepository<Routes,Long> {
    boolean existsByName(String name);
    Page<Routes> findAllByDepartureStation(String departure, Pageable pageable);
    Page<Routes> findAllByArrivalStation(String arrival, Pageable pageable);
}
