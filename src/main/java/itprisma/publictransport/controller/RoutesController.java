package itprisma.publictransport.controller;

import itprisma.publictransport.model.Routes;
import itprisma.publictransport.service.RouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/api/v1/routes")
public class RoutesController {

    @Autowired
    RouteService routeService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Routes createRoute(@Valid @RequestBody Routes routes){
        return routeService.createRoutes(routes);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Page<Routes> findAll(@RequestParam(required = false, defaultValue = "0") @Min(value = 0) int index,
                                @RequestParam(required = false, defaultValue = "100") @Min(value = 0) int limit) {
        return routeService.findAll(index, limit);
    }

    @GetMapping("/departures/{departure}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Page<Routes> findAllByDeparture(@RequestParam(required = false, defaultValue = "0") @Min(value = 0) int index,
                                           @RequestParam(required = false, defaultValue = "100") @Min(value = 0) int limit,
                                           @PathVariable("departure") String departure) {
        return routeService.findAllByDeparture(index, limit, departure);
    }

    @GetMapping("/arrival/{arrival}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Page<Routes> findAllByArrival(@RequestParam(required = false, defaultValue = "0") @Min(value = 0) int index,
                                         @RequestParam(required = false, defaultValue = "100") @Min(value = 0) int limit,
                                           @PathVariable("arrival") String arrival) {
        return routeService.findAllByArrival(index, limit, arrival);
    }

    @GetMapping("/{id-routes}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Routes findRouteById(@PathVariable("id-routes") @NotNull Long routesId) {
        return routeService.findById(routesId);
    }

    @PutMapping("/{id-routes}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Routes updateRoute(@PathVariable("id-routes") @NotNull Long routesId,
                             @RequestBody Routes routes){
        routes.setId(routesId);
        return routeService.updateRoutes(routes);
    }

    @DeleteMapping("/{id-routes}")
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void deleteRouteById(@PathVariable("id-routes") @NotNull Long routesId) {
        routeService.deleteRoute(routesId);
    }
}
