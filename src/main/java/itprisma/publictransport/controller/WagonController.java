package itprisma.publictransport.controller;

import itprisma.publictransport.model.Wagon;
import itprisma.publictransport.service.WagonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/api/v1")
public class WagonController {

    @Autowired
    private WagonService wagonService;

    @PostMapping("/wagons")
    @ResponseStatus(HttpStatus.CREATED)
    public Wagon createTrain(@Valid @RequestBody Wagon wagon){
        return wagonService.createWagon(wagon);
    }

    @GetMapping("/wagons")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Page<Wagon> findAll(@RequestParam(required = false, defaultValue = "0") @Min(value = 0) int index,
                               @RequestParam(required = false, defaultValue = "100") @Min(value = 0) int limit) {
        return wagonService.findAll(index, limit);
    }

    @GetMapping("/{id-wagon}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Wagon findTrainById(@PathVariable("id-wagon") @NotNull Long wagonId) {
        return wagonService.findById(wagonId);
    }

    @PutMapping("/{id-wagon}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Wagon updateTrain(@PathVariable("id-wagon") @NotNull Long wagonId,
                             @RequestBody Wagon wagon){
        wagon.setId(wagonId);
        return wagonService.updateWagon(wagon);
    }

    @GetMapping("/trains/{id-train}/wagons")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Page<Wagon> findAllByTrain(@RequestParam(required = false, defaultValue = "0") @Min(value = 0) int index,
                                      @RequestParam(required = false, defaultValue = "100") @Min(value = 0) int limit,
                                      @PathVariable("id-train") @NotNull Long trainId) {
        return wagonService.findByTrain(index, limit, trainId);
    }

    @GetMapping("/wagons/{power-supply}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Page<Wagon> findAllByPowerSupply(@RequestParam(required = false, defaultValue = "0")@Min(value = 0) int index,
                                            @RequestParam(required = false, defaultValue = "100") @Min(value = 0) int limit,
                                            @PathVariable("power-supply") Wagon.PowerSupply powerSupply) {
        return wagonService.findByPowerSupplyIsLike(index, limit, powerSupply);
    }
}
