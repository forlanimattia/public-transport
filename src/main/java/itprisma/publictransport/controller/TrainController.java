package itprisma.publictransport.controller;

import itprisma.publictransport.model.Train;
import itprisma.publictransport.service.TrainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/api/v1/trains")
public class TrainController {

    @Autowired
    private TrainService trainService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Train createTrain(@Valid @RequestBody Train train){
        return trainService.createTrain(train);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Page<Train> findAll(@RequestParam(required = false, defaultValue = "0") @Min(value = 0) int index,
                               @RequestParam(required = false, defaultValue = "100") @Min(value = 0) int limit) {
        return trainService.findAll(index, limit);
    }

    @GetMapping("/{id-train}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Train findTrainById(@PathVariable("id-train") @NotNull Long trainId) {
        return trainService.findById(trainId);
    }

    @PutMapping("/{id-train}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Train updateTrain(@PathVariable("id-train") @NotNull Long trainId,
                             @RequestBody Train train){
        train.setId(trainId);
        return trainService.updateTrain(train);
    }

    @PutMapping("/{id-train}/routes/{id-routes}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Train updateTrainAddingRoutes(@PathVariable("id-train") @NotNull Long trainId,
                                        @PathVariable("id-routes") Long routesId){
        return trainService.addRoutesInTrain(trainId, routesId);
    }

    @GetMapping("/arrival/{arrival}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Page<Train> findTrainByArrival(@RequestParam(required = false, defaultValue = "0") @Min(value = 0) int index,
                                          @RequestParam(required = false, defaultValue = "100") @Min(value = 0) int limit,
                                          @PathVariable("arrival") String arrival) {
        return trainService.findTrainByArrival(arrival, index, limit);
    }
}
