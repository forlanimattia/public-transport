package itprisma.publictransport.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Slf4j
@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends RuntimeException {

    public NotFoundException(Long id) {
        super(String.format("Resource with ID [%s] not found.", id));
        log.warn("Resource with ID [{}] not found.", id);
    }
}
