package itprisma.publictransport.service;

import itprisma.publictransport.component.ConfigurationComponent;
import itprisma.publictransport.model.Train;
import itprisma.publictransport.model.Wagon;
import itprisma.publictransport.persistence.WagonRepository;
import itprisma.publictransport.utils.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class WagonService extends BaseService{
    
    @Autowired
    WagonRepository wagonRepository;

    @Autowired
    TrainService trainService;

    public WagonService(ConfigurationComponent config) {
        super(config);
    }

    public Page<Wagon> findAll(int index, int limit) {
        return wagonRepository.findAll(PageRequest.of(index, limit));
    }

    public Wagon updateWagon(Wagon wagon) {
        Wagon wagonToUpdate = findById(wagon.getId());
        wagonToUpdate.partialUpdate(wagon);
        return wagonRepository.save(wagonToUpdate);
    }

    public Wagon createWagon(Wagon wagon) {
        return wagonRepository.save(wagon);
    }

    public Wagon findById(Long id) {
        return wagonRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(id));
    }

    public Page<Wagon> findByTrain(int index, int limit, Long trainId) {
        Train train = trainService.findById(trainId);
        return wagonRepository.findByTrain(train,PageRequest.of(index, limit));
    }

    public Page<Wagon> findByPowerSupplyIsLike(int index, int limit, Wagon.PowerSupply powerSupply) {

        return wagonRepository.findByPowerSupply(PageRequest.of(index, limit),powerSupply);
    }


}
