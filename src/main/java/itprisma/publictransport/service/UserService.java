package itprisma.publictransport.service;

import itprisma.publictransport.domain.Role;
import itprisma.publictransport.domain.User;

import java.util.List;

public interface UserService {
    User saveUser(User user);
    Role saveRole(Role role);
    void addRoleToUser(String username, String roleName);
    User getUser(String username);
    List<User> getUsers();
}
