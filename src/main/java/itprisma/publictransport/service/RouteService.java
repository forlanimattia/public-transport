package itprisma.publictransport.service;

import itprisma.publictransport.component.ConfigurationComponent;
import itprisma.publictransport.model.Routes;
import itprisma.publictransport.persistence.RoutesRepository;
import itprisma.publictransport.utils.ConflictException;
import itprisma.publictransport.utils.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class RouteService extends BaseService{

    @Autowired
    RoutesRepository routesRepository;

    public RouteService(ConfigurationComponent config) {
        super(config);
    }

    public Routes findById(Long id){
        return routesRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(id));
    }

    public Page<Routes> findAll(int index, int limit){
        return routesRepository.findAll(PageRequest.of(index, handleLimit(limit)));
    }

    public Page<Routes> findAllByDeparture(int index, int limit, String departure){
        return routesRepository.findAllByDepartureStation(departure, PageRequest.of(index, handleLimit(limit)));
    }

    public Page<Routes> findAllByArrival(int index, int limit, String arrival){
        return routesRepository.findAllByArrivalStation(arrival, PageRequest.of(index, handleLimit(limit)));
    }

    public Routes createRoutes(Routes routes){
        if(routesRepository.existsByName(routes.getName())){
            throw new ConflictException("This routes already exists", routes.getName());
        }
        return routesRepository.save(routes);
    }

    public Routes updateRoutes(Routes routes){
        Routes routesToUpdate = findById(routes.getId());
        routesToUpdate.partialUpdate(routes);
        return routesRepository.save(routesToUpdate);
    }

    public void deleteRoute(Long routesId) {
        Routes routeToDelete = findById(routesId);
        routesRepository.delete(routeToDelete);
    }
}
