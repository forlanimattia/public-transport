package itprisma.publictransport.service;

import itprisma.publictransport.component.ConfigurationComponent;

public class BaseService {

    private final ConfigurationComponent config;

    public BaseService(ConfigurationComponent config) {
        this.config = config;
    }

    protected int handleLimit(int limit) {
        int defaultLimit = config.getDefaultPageLimit();
        return Math.min(limit, defaultLimit);
    }
}
