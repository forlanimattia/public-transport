package itprisma.publictransport.service;

import itprisma.publictransport.component.ConfigurationComponent;
import itprisma.publictransport.model.Routes;
import itprisma.publictransport.model.Train;
import itprisma.publictransport.persistence.TrainRepository;
import itprisma.publictransport.utils.ConflictException;
import itprisma.publictransport.utils.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;


@Service
public class TrainService extends BaseService{

    @Autowired
    private TrainRepository trainRepository;

    @Autowired
    private RouteService routeService;

    public TrainService(ConfigurationComponent config) {
        super(config);
    }

    public Train findById(Long id){
        return trainRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(id));
    }

    public Page<Train> findAll(int index, int limit){
        return trainRepository.findAll(PageRequest.of(index, handleLimit(limit)));
    }

    public Train createTrain(Train train){
        if(trainRepository.existsByName(train.getName())){
            throw new ConflictException("Train with this name already exists", train.getName());
        }
        return trainRepository.save(train);
    }

    public Train updateTrain(Train train){
        Train trainToUpdate = findById(train.getId());
        trainToUpdate.partialUpdate(train);
        if (trainRepository.existsByNameAndIdIsNot(train.getName(), train.getId())){
            throw new ConflictException("Train with this name already exists", train.getName());
        }
        return trainRepository.save(trainToUpdate);
    }

    public Train addRoutesInTrain(Long trainId, Long routesId) {
        Routes routesToAdd = routeService.findById(routesId);
        Train train = findById(trainId);
        if (train.getRoutes().contains(routesToAdd)){
            throw new ConflictException(routesId.toString(),"This train already have this route");
        }
        train.getRoutes().add(routesToAdd);
        return trainRepository.save(train);
    }

    public Page<Train> findTrainByArrival(String arrival, int index, int limit) {
        return trainRepository.findAllByRoutesIsLike(arrival, PageRequest.of(index,handleLimit(limit)));

    }
}
