package itprisma.publictransport.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.lang.reflect.Field;

@Entity(name = "Wagon")
@Table(name = "wagon")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Wagon {

    @Id
    @Null
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name",
            columnDefinition = "ENUM('passeggeri', 'letto', 'locomotore', 'ristorante')")
    @Enumerated(EnumType.STRING)
    @NotNull
    private Name name;

    @Column(name = "seats")
    private Integer seats;

    @Column(name = "bed_type",
            columnDefinition = "ENUM('single', 'doubles')")
    @Enumerated(EnumType.STRING)
    private BedType bedType;

    @Column(name = "tables")
    private Integer tables;

    @Column(name = "kitchen_type",
            columnDefinition = "ENUM('modern', 'old', 'hybrid')")
    @Enumerated(EnumType.STRING)
    private KitchenType kitchenType;

    @Column(name = "power_supply",
            columnDefinition = "ENUM('electric', 'diesel', 'gas')")
    @Enumerated(EnumType.STRING)
    private PowerSupply powerSupply;

    @Column(name = "classes",
            columnDefinition = "ENUM('first', 'second', 'third', 'economy')")
    @Enumerated(EnumType.STRING)
    private Classes classes;

    @ManyToOne
    @JoinColumn(name = "id_train", referencedColumnName = "id")
    @JsonBackReference("train-wagon")
    @NotNull
    private Train train;

    @Column(name = "size")
    @NotNull
    private Long size;

    public enum Classes {
        first,
        second,
        third,
        economy
    }

    public enum Name {
        passeggeri,
        letto,
        locomotore,
        ristorante
    }

    public enum PowerSupply {
        electric,
        diesel,
        gas
    }

    public enum KitchenType {
        modern,
        old,
        hybrid
    }

    public enum BedType {
        single,
        doubles
    }

    public void partialUpdate(Wagon wagonToCompare) {
        for (Field w : wagonToCompare.getClass().getDeclaredFields()) {
            try {
                if (w.get(wagonToCompare) != null) {
                    w.set(this, w.get(wagonToCompare));
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }
}
