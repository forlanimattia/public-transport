package itprisma.publictransport.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "Train")
@Table(name = "train")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
//@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Train {

    @Id
    @Null
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    @NotBlank
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "type",
            columnDefinition = "ENUM('regionale', 'intercity', 'freccia_rossa')")
    @NotNull
    private Type type;

    @OneToMany(mappedBy = "train", fetch = FetchType.LAZY)
    @JsonManagedReference("train-wagon")
    List<Wagon> wagons = new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "train_routes",
                joinColumns = @JoinColumn(name = "id_train"),
                inverseJoinColumns = @JoinColumn(name = "id_routes"))
    @JsonIgnoreProperties(value = "trains")
    private List<Routes> routes = new ArrayList<>();

    public enum Type{
        regionale,
        intercity,
        freccia_rossa
    }

    public void partialUpdate(Train trainToCompare) {
        for (Field t : trainToCompare.getClass().getDeclaredFields()) {
            try {
                if (t.get(trainToCompare) != null) {
                    t.set(this, t.get(trainToCompare));
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }
}
