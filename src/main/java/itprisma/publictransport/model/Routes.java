package itprisma.publictransport.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "Routes")
@Table(name = "routes")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Routes {

    @Id
    @Column(name = "id")
    @Null
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    @NotBlank
    private String name;

    @Column(name = "departure_station")
    @NotBlank
    private String departureStation;

    @Column(name = "arrival_Station")
    @NotBlank
    private String arrivalStation;

    @Column(name = "length")
    @NotNull
    private Long length;

    @Column(name = "binary_type")
    @NotBlank
    private String binaryType;

    @ManyToMany(mappedBy = "routes")
    @JsonIgnoreProperties(value = "routes")
    private List<Train> trains = new ArrayList<>();

    public void partialUpdate(Routes routesToCompare) {
        for (Field r : routesToCompare.getClass().getDeclaredFields()) {
            try {
                if (r.get(routesToCompare) != null) {
                    r.set(this, r.get(routesToCompare));
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Routes routes = (Routes) o;

        return id != null ? id.equals(routes.id) : routes.id == null;
    }
}
